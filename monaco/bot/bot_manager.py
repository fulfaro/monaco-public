#!/usr/bin/env python
import sys
from monaco.bot.bot_base import BotBase


def main(argv):
    bot_base = BotBase(argv[1], argv[2])    
    print bot_base.start()
       
if __name__ == "__main__":
    main(sys.argv)
