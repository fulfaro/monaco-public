#!/usr/bin/env python
import sys
from monaco.bot.bot_group import BotGroup


def main(argv):
    bot_group = BotGroup(argv[1])
    print bot_group.start_bots()


if __name__ == "__main__":
    main(sys.argv)
