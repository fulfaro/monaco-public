from monaco.data.bot_data_manager import BotDataManager
from monaco.process.process_manager import ProcessManager
import json

process_manager = ProcessManager()
bot_data_manager = BotDataManager()


class BotBase:
    namespace = ''
    code = ''
    configuration = {}
    
    def __init__(self, namespace, code):
        self.code = code
        self.namespace = namespace
        self._fill_configuration()
        
    def __unicode__(self):
        pass
        
    def __str__(self):
        pass
    
    def _fill_configuration(self):
        bot_config = bot_data_manager.select_by_bot_code(self.namespace, self.code)
        self.method_code = bot_config["method_code"]
        self.method_namespace = bot_config["method_namespace"]
        self.configuration = bot_config["configuration"]
        
    def start(self):
        return process_manager.execute_with_key(self.method_namespace, self.method_code, json.dumps(self.configuration))
