from monaco.data.bot_data_manager import BotDataManager
from monaco.process.process_manager import ProcessManager
import json

process_manager = ProcessManager()
bot_data_manager = BotDataManager()


class BotGroup:
    group = ''

    def __init__(self, group):
        self.group = group

    def __unicode__(self):
        pass

    def __str__(self):
        pass

    def start_bots(self):
        list_bot = bot_data_manager.select_by_bot_group(self.group)
        for bot_data in list_bot:
            print process_manager.execute_with_key(bot_data["method_namespace"], bot_data["method_code"],
                                             json.dumps(bot_data["configuration"]))
