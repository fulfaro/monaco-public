import traceback
import logging

logger = logging.getLogger('monaco_server')
logger.setLevel(logging.ERROR)
fh = logging.FileHandler('monaco_server.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

class ServerLogger():
    def make_log_error(self):        
        logger.error(traceback.format_exc().splitlines())        
        print traceback.format_exc()