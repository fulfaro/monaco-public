from monaco.data.method_data_manager import MethodDataManager
import json

method_data_manager = MethodDataManager()


class ProcessManager:

    def __init__(self):
        pass
    
    def need_token(self):
        pass
    
    def validate_access(self):
        pass
        
    def execute(self, namespace, method_code, params):        
        method_configuration = method_data_manager.select_by_method_code(namespace, method_code)                
        
        if params != '':
            params = json.loads(params)
        elif params == '':
            params = {}
        else:
            params = {}
        
        if method_configuration != None:                    
            import_string = "from " +  str(method_configuration["package"]) + " import " + str(method_configuration["class"])
            exec import_string
            
            class_instance = eval(str(method_configuration["class"]))()        
            result = getattr(class_instance, str(method_configuration["method"]))(**params)                
            return result
        else:
            return "NO EXISTE"
        
    def execute_with_key(self, namespace, method_code, params):        
        method_configuration = method_data_manager.select_by_method_code(namespace, method_code)
        if params != '':
            params = json.loads(params)
        elif params == '':
            params = {}
        else:
            params = {}
        
        params.update({'master_key': method_configuration['master_key']})
        
        if method_configuration is not None:
            import_string = "from " + str(method_configuration["package"]) + " import " + str(method_configuration["class"])
            exec import_string
            
            class_instance = eval(str(method_configuration["class"]))()        
            result = getattr(class_instance, str(method_configuration["method"]))(**params)                
            return result
        else:
            return "NO EXISTE"
