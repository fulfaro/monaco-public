#!/usr/bin/env python
from flask import Flask, request
from monaco.server.response.responses import make_unauthorized_response, make_server_error_response
from monaco.data.data_manager import DataManager
from monaco.security.access import AccessManager

app = Flask(__name__)
        
################ routes ###############
@app.route('/entity/<database>/<collection>', methods=['POST'])
def create(database, collection):
    try:        
        if AccessManager(database, collection).validate_access_request(request.headers, "C") == False:
            return make_unauthorized_response()
        
        data = DataManager(database, collection)
        return data.create(request.data)
    except:
        return make_server_error_response()
    
@app.route('/entity/<database>/<collection>', methods=['GET'])
def select_collection(database, collection):
    try:
        if AccessManager(database, collection).validate_access_request(request.headers, "R") == False:
            return make_unauthorized_response()
            
        data = DataManager(database, collection)
        
        if request.query_string != '':
            return data.select_find(request.query_string)
        else:
            return data.select_collection()           
    except:
        return make_server_error_response()
    
@app.route('/entity/<database>/<collection>/<entity_id>', methods=['GET'])
def select(database, collection,entity_id):    
    try:
        if AccessManager(database, collection).validate_access_request(request.headers, "R") == False:
            return make_unauthorized_response()
            
        data = DataManager(database, collection)
        return data.select(entity_id)
    except:
        return make_server_error_response()                 
    
@app.route('/entity/<database>/<collection>', methods=['DELETE'])
def delete_collection(database, collection):    
    try:
        if AccessManager(database, collection).validate_access_request(request.headers, "D") == False:
            return make_unauthorized_response()
            
        DataManager(database, collection).delete_collection()
        return str(True)
    except:
        make_server_error_response()
        return str(False)
    
@app.route('/entity/<database>/<collection>/<entity_id>', methods=['DELETE'])
def delete(database, collection,entity_id):    
    try:
        if AccessManager(database, collection).validate_access_request(request.headers, "D") == False:
            return make_unauthorized_response()
            
        DataManager(database, collection).delete(entity_id)
        return str(True)
    except:
        make_server_error_response()
        return str(False)
    
@app.route('/entity/<database>/<collection>/<entity_id>', methods=['PUT'])
def update(database,collection,entity_id):    
    try:
        if AccessManager(database, collection).validate_access_request(request.headers, "U") == False:
            return make_unauthorized_response()
            
        DataManager(database, collection).update(entity_id, request.data)
        return str(True)
    except:
        make_server_error_response()
        return str(False)
    
if __name__ == '__main__':
    app.run(debug=True, port=5002)