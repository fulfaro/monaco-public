from flask import make_response
from monaco.log.server_logger import ServerLogger

logger = ServerLogger()

def make_unauthorized_response():
    response = make_response()
    response.status_code = 401
    response.data = response.status
    return response
    
def make_server_error_response():
    logger.make_log_error()
    response = make_response()
    response.status_code = 500
    response.data = response.status
    return response