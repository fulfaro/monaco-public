#!/usr/bin/env python
from flask import Flask, request
from monaco.server.response.responses import make_unauthorized_response, make_server_error_response
from monaco.security.token import TokenManager
from monaco.data.data_manager import DataManager
from monaco.security.access import AccessManager
from monaco.process.process_manager import ProcessManager
from monaco.security.access import AccessProcessManager

app = Flask(__name__)
token_manager = TokenManager()
process_manager = ProcessManager()


def validate_request(headers):
    if "X-LOGIN" in headers and "X-PASSWORD" in headers:
        return True
    return False


# ############### SECURITY MANAGER ###############
@app.route('/security', methods=['POST'])
def create_security():
    try:
        if validate_request(request.headers):
            if token_manager.validate_user(request.headers["X-LOGIN"], request.headers["X-PASSWORD"]) is True:
                return token_manager.generate(request.headers["X-LOGIN"])

        return make_unauthorized_response()
    except:
        return make_server_error_response()


# ############### DATA MANAGER ###############
@app.route('/entity/<database>/<collection>', methods=['POST'])
def create(database, collection):
    try:
        if AccessManager(database, collection).validate_access_request(request.headers, "C") is False:
            return make_unauthorized_response()

        data = DataManager(database, collection)
        return data.create(request.data)
    except:
        return make_server_error_response()


@app.route('/entity/<database>/<collection>', methods=['GET'])
def select_collection(database, collection):
    try:
        if AccessManager(database, collection).validate_access_request(request.headers, "R") is False:
            return make_unauthorized_response()

        data = DataManager(database, collection)

        if request.query_string != '':
            return data.select_find(request.query_string)
        else:
            return data.select_collection()
    except:
        return make_server_error_response()


@app.route('/entity/<database>/<collection>/<entity_id>', methods=['GET'])
def select(database, collection, entity_id):
    try:
        if AccessManager(database, collection).validate_access_request(request.headers, "R") is False:
            return make_unauthorized_response()

        data = DataManager(database, collection)
        return data.select(entity_id)
    except:
        return make_server_error_response()


@app.route('/entity/<database>/<collection>', methods=['DELETE'])
def delete_collection(database, collection):
    try:
        if AccessManager(database, collection).validate_access_request(request.headers, "D") is False:
            return make_unauthorized_response()

        DataManager(database, collection).delete_collection()
        return str(True)
    except:
        make_server_error_response()
        return str(False)


@app.route('/entity/<database>/<collection>/<entity_id>', methods=['DELETE'])
def delete(database, collection, entity_id):
    try:
        if AccessManager(database, collection).validate_access_request(request.headers, "D") is False:
            return make_unauthorized_response()

        DataManager(database, collection).delete(entity_id)
        return str(True)
    except:
        make_server_error_response()
        return str(False)


@app.route('/entity/<database>/<collection>/<entity_id>', methods=['PUT'])
def update(database, collection, entity_id):
    try:
        if AccessManager(database, collection).validate_access_request(request.headers, "U") is False:
            return make_unauthorized_response()

        DataManager(database, collection).update(entity_id, request.data)
        return str(True)
    except:
        make_server_error_response()
        return str(False)


# ############### PROCESS MANAGER ###############
@app.route('/method/<namespace>/<method_code>', methods=['POST'])
def execute_method(namespace, method_code):
    try:
        if AccessProcessManager(namespace, method_code).validate_access_process(request.headers) is False:
            return make_unauthorized_response()

        return process_manager.execute(namespace, method_code, request.data)
    except:
        return make_server_error_response()


if __name__ == '__main__':
    app.run(debug=True, port=5001)