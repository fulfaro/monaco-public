#!/usr/bin/env python
from flask import Flask, request
from monaco.security.token import TokenManager
from monaco.server.response.responses import make_unauthorized_response, make_server_error_response

app = Flask(__name__)
token_manager = TokenManager()

def validate_request(headers):
    if "X-LOGIN" in headers and "X-PASSWORD" in headers:
        return True
    return False

@app.route('/security', methods=['POST'])
def create():
    try:        
        if validate_request(request.headers):
            if token_manager.validate_user(request.headers["X-LOGIN"], request.headers["X-PASSWORD"]) == True:
                return token_manager.generate(request.headers["X-LOGIN"])
            
        return make_unauthorized_response()
    except:
        return make_server_error_response()

if __name__ == '__main__':
    app.run(debug=True, port=5001)