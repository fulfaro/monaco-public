#!/usr/bin/env python
from flask import Flask, request
from monaco.server.response.responses import make_unauthorized_response, make_server_error_response
from monaco.process.process_manager import ProcessManager
from monaco.security.access import AccessProcessManager

app = Flask(__name__)

process_manager = ProcessManager()

@app.route('/method/<namespace>/<method_code>', methods=['POST'])
def execute_method(namespace, method_code):
    try:
        if AccessProcessManager(namespace, method_code).validate_access_process(request.headers) == False:
            return make_unauthorized_response()
        
        return process_manager.execute(namespace, method_code, request.data)
    except:
        return make_server_error_response()
    
    
if __name__ == '__main__':
    app.run(debug=True, port=5003)
