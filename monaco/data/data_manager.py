from pymongo import ASCENDING, DESCENDING
from bson import json_util, objectid
import json
from monaco.data import MONGO_CLIENT


class DataManager:
    database = ''
    collection = ''

    def __init__(self, database, collection):
        self.database = database
        self.collection = collection
        
    def __unicode__(self):
        return self.database + '-' + self.collection
    
    def __str__(self):
        return self.database + '-' + self.collection 

    def create(self, resource):
        resource = json.loads(resource)
        return str(MONGO_CLIENT[self.database][self.collection].insert(resource))
    
    def select_collection(self):
        result = MONGO_CLIENT[self.database][self.collection].find()
        return str(json.dumps(list(result), indent=4, default=json_util.default))
    
    def select(self, entity_id):
        result = MONGO_CLIENT[self.database][self.collection].find({'_id': objectid.ObjectId(entity_id)})
        if result.count() > 0:        
            return str(json.dumps(result[0], indent=4, default=json_util.default))
        else:
            return ''
            
    def select_find(self, query_string):
        """
        Igual a : =
        No Igual a : [n]=
        Mayor que : [gt]=
        Mayor o igual que : [gte]=
        Menor que : [lt]=
        Menor o igual que : [lte]=
        *In - Varios
        *Bettewn
        *like
        *OPERATORS = AND, OR
        NOTA: PARA BUSCAR POR PARAMETROS QUE NO SEAN STRING SE DEBEN HACER PARSE, PARA ESTO ES NECESARIO
        SABER QUE TIPO DE DATOS ES, ESO SE REALIZARA CUANDO SE SEPA QUE TIPO DE DATOS ES CON EL SCHEME DE LA TABLA
        """
        # TODO: CON SCHEME SE PUEDE PARSEAR LA BUSQUEDA AL TIPO DE DATO NECESARIO
        params = query_string.split("=")
        field = params[0].split("[")[0]
        operator = params[0][params[0].find("["):len(params[0])]
        
        if operator == "[n]":
            result = MONGO_CLIENT[self.database][self.collection].find({field: {"$ne": params[1]}})
        elif operator == "[gt]":
            result = MONGO_CLIENT[self.database][self.collection].find({field: {"$gt": int(params[1])}})
        elif operator == "[gte]":
            result = MONGO_CLIENT[self.database][self.collection].find({field: {"$gte": int(params[1])}})
        elif operator == "[lt]":
            result = MONGO_CLIENT[self.database][self.collection].find({field: {"$lt": int(params[1])}})
        elif operator == "[lte]":
            result = MONGO_CLIENT[self.database][self.collection].find({field: {"$lte": int(params[1])}})
        else:
            result = MONGO_CLIENT[self.database][self.collection].find({field: params[1]})
        
        if result.count() > 0:
            return str(json.dumps(list(result), indent=4, default=json_util.default))
        else:
            return str([])
       
    def delete_collection(self):
        # TODO: Para la siguiente version debe ser remove({})
        MONGO_CLIENT[self.database][self.collection].drop()
        
    def delete(self, entity_id):
        MONGO_CLIENT[self.database][self.collection].remove({'_id': objectid.ObjectId(entity_id)})
        
    def update(self, entity_id, resource):
        resource = json.loads(resource)
        MONGO_CLIENT[self.database][self.collection].update({'_id': objectid.ObjectId(entity_id)}, resource, True)
        return self.select(entity_id)
    
    def find_min_element(self, value):
        result = MONGO_CLIENT[self.database][self.collection].find().sort(value, ASCENDING).limit(1)
        if result.count() > 0:        
            return result[0]
        else:
            return {}
        
    def find_max_element(self, value):
        result = MONGO_CLIENT[self.database][self.collection].find().sort(value, DESCENDING).limit(1)
        if result.count() > 0:        
            return result[0]
        else:
            return {}
    
    def count_collection(self):
        return MONGO_CLIENT[self.database][self.collection].find().count()
    
    def select_one_query(self, query):
        result = MONGO_CLIENT[self.database][self.collection].find(query)
        if result.count() > 0:        
            return str(json.dumps(result[0], indent=4, default=json_util.default))
        else:
            return {}
        
    def select_collection_query(self, query):
        result = MONGO_CLIENT[self.database][self.collection].find(query)
        if result.count() > 0:        
            return result
        else:
            return {}