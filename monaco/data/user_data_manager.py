import datetime
from monaco.data import MONGO_CLIENT, MASTER_DATA_BASE


class UserDataManager:
    database = MASTER_DATA_BASE
    collection = 'monaco_user'
    session = "monaco_token"
    
    def __init__(self):
        pass
        
    def __unicode__(self):
        pass
        
    def __str__(self):
        pass
        
    def validate_user(self, login, password):
        result = MONGO_CLIENT[self.database][self.collection].find_one({'login': login, 'password': password})
        if result is not None:
            return True
        else:
            return False
            
    def save_session(self, login, token):
        date = datetime.datetime.utcnow()
        delta = datetime.timedelta(hours=1)
        MONGO_CLIENT[self.database][self.session].insert({'login': login, 'token': token, "created_date": date,
                                                          "expiration_date": date + delta})
    
    def get_session(self, token):
        result = MONGO_CLIENT[self.database][self.session].find_one({'token': token})
        if result is None:
            return False, ""
        else:
            return True, result["login"]
