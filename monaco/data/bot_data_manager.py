from monaco.data import MONGO_CLIENT, MASTER_DATA_BASE


class BotDataManager:
    database = MASTER_DATA_BASE
    collection = 'monaco_bot_configuration'
    
    def __init__(self):
        pass
        
    def __unicode__(self):
        pass
        
    def __str__(self):
        pass

    def select_by_bot_group(self, group):
        return MONGO_CLIENT[self.database][self.collection].find({'group': group})

    def select_by_bot_code(self, namespace, bot_code):
        return MONGO_CLIENT[self.database][self.collection].find_one({'namespace': namespace, 'code': bot_code})
    
    def select_configuration_by_code(self, namespace, bot_code):
        return MONGO_CLIENT[self.database][self.collection].find_one({'namespace': namespace,
                                                                      'code': bot_code})['configuration']
