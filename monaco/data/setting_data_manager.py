from monaco.data import MONGO_CLIENT, MASTER_DATA_BASE


class SettingDataManager:
    database = MASTER_DATA_BASE
    collection = 'monaco_settings'
    
    def __init__(self):
        pass
        
    def __unicode__(self):
        pass
        
    def __str_(self):
        pass
    
    def find_config_by_master_key(self, master_key):
        return MONGO_CLIENT[self.database][self.collection].find_one({'master_key': master_key})['configuration']
