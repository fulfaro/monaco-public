from monaco.data import MONGO_CLIENT, MASTER_DATA_BASE


class MethodDataManager:
    database = MASTER_DATA_BASE
    collection = 'monaco_method_configuration'
    
    def __init__(self):
        pass
        
    def __unicode__(self):
        pass
        
    def __str__(self):
        pass
        
    def select_by_method_code(self, namespace, method_code):
        return MONGO_CLIENT[self.database][self.collection].find_one({'namespace': namespace, 'code': method_code})
