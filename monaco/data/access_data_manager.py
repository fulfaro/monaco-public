from monaco.data import MONGO_CLIENT, MASTER_DATA_BASE


class AccessDataManager:
    database = MASTER_DATA_BASE
    collection = 'monaco_access_data'
    
    def __init__(self):
        pass
        
    def __unicode__(self):
        return self.database + '-' + self.collection
        
    def __str__(self):
        return self.database + '-' + self.collection
    
    # TODO: ALMACENAR EN CACHE - EVALUAR PARA LAS DEMAS FUNCIONES
    def find_access_list(self, database_find, collection_find):
        return MONGO_CLIENT[self.database][self.collection].find_one({'database': database_find,
                                                                      'collection': collection_find})
        
    def find_access_method(self, database_find, collection_find, login, method):
        return MONGO_CLIENT[self.database][self.collection].find({"database": database_find,
                                                                  "collection": collection_find,
                                                                  "permissions.login": login,
                                                                  "permissions.access.type": method})
        
    def find_level_for_method(self, database_find, collection_find, method):
        result = MONGO_CLIENT[self.database][self.collection].find_one({'database': database_find,
                                                                        'collection': collection_find},
                                                                       {"configuration": 1})
        if result == None:
            return "private"
        else:
            for item in result["configuration"]:                
                if item["type"] == method:                    
                    return item["level"]
        return "private"


class AccessMethodDataManager:
    database = 'monaco_master'
    collection = 'access_method'
    
    def __init__(self):
        pass
        
    def __unicode__(self):
        return self.database + '-' + self.collection
        
    def __str__(self):
        return self.database + '-' + self.collection
    
    def find_level_for_method(self, namespace, method_code, method):
        result = MONGO_CLIENT[self.database][self.collection].find_one({"namespace": namespace, "code": method_code},
                                                                       {"configuration": 1})
        if result is None:
            return "private"
        else:
            for item in result["configuration"]:                
                if item["type"] == method:                    
                    return item["level"]
        return "private"
        
    def find_access_method(self, login, namespace, method_code):
        result = MONGO_CLIENT[self.database][self.collection].find_one({"namespace": namespace, "code": method_code,
                                                                        "permissions.login": login}, {"permissions": 1})
        if result is None:
            return False
        else:
            for item in result["permissions"]:                
                if item["login"] == login:
                    for access in item["access"]:
                        if access["type"] == "E":                            
                            return True
                        else:
                            return False
                else:
                    return False                    
        return True
