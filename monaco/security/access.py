from monaco.data.access_data_manager import AccessDataManager, AccessMethodDataManager
from monaco.security.token import TokenManager

access_manager = AccessDataManager()
token_manager = TokenManager()
access_method_data_manager = AccessMethodDataManager()

class AccessManager():    
    database = ""
    collection = ""
    
    def __init__(self, database, collection):
        self.database = database
        self.collection = collection
        
    def need_token(self, method):
        level = access_manager.find_level_for_method(self.database, self.collection, method)
        if level == "private":
            return True
        else:
            return False
            
    def validate_data_access(self, login, method):        
        access_list = access_manager.find_access_method(self.database, self.collection, login, method)
        if access_list == None:
            return True
        else:
            if access_list.count() > 0:
                return True
            else:
                return False
                
    def validate_access_request(self, headers, method):
        if self.need_token(method) == True:            
            if "X-TOKEN" in headers:
                valid_token, login = token_manager.get_token(headers["X-TOKEN"])
                if valid_token == True:
                    if self.validate_data_access(login, method) == False:
                        return False
                else:    
                    return False
            else:                
                return False
        return True
    
class AccessProcessManager():
    namespace = ''
    method_code = ''
    
    def __init__(self, namespace, method_code):
        self.namespace = namespace
        self.method_code = method_code
        
    def __unicode__(self):
        pass
        
    def __str__(self):
        pass                          
    
    def need_token(self, method):
        level = access_method_data_manager.find_level_for_method(self.namespace, self.method_code, method)
        if level == "private":
            return True
        else:
            return False
    
    def validate_access_process(self, headers):
        if self.need_token('E') == True:
            if "X-TOKEN" in headers:
                valid_token, login = token_manager.get_token(headers["X-TOKEN"])
                if valid_token:
                    if access_method_data_manager.find_access_method(login, self.namespace, self.method_code) == False:
                        return False
                else:
                    return False
            else:
                return False
        return True