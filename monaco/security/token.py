from monaco.data.user_data_manager import UserDataManager
import uuid
user_manager = UserDataManager()
    
class TokenManager():
    def generate(self, login):        
        uid = str(uuid.uuid4())
        user_manager.save_session(login, uid)
        return uid
        
    def validate_user(self, login, password):        
        return user_manager.validate_user(login, password)
        
    def get_token(self, token):        
        return user_manager.get_session(token)