#!/usr/bin/env python
from distutils.core import setup

setup(name='Monaco',
      version='0.2',
      description='Monaco Server',
      author='Juan Carlos Romero',
      author_email='juan.romero@pointer.ms',
      url='http://www.pointer.ms/',
      packages=[
            'monaco',
            'monaco.data',
            'monaco.server',
            'monaco.server.response',
            'monaco.security',
            'monaco.log',
            'monaco.process',
            'monaco.test',
            'monaco.bot',
            'monaco.util',
            'kublai',
            'kublai.bots'],
      requires=[
            'flask',
            'bson',
            'pymongo',
            'facepy',
            'tweepy'
      ],)

