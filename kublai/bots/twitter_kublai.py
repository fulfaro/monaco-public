import tweepy
from tweepy.parsers import JSONParser
from monaco.data.setting_data_manager import SettingDataManager
from monaco.data.data_manager import DataManager
import json
import datetime
from time import mktime
import time
import sys

setting_datamanager = SettingDataManager()


class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return int(mktime(obj.timetuple()))
        return json.JSONEncoder.default(self, obj)


def include_datetime(result):
    _now = datetime.datetime.now()
    result.update({'bot_datetime': json.dumps(_now, cls=DateTimeEncoder)})
    result.update({'bot_year': _now.year})
    result.update({'bot_month': _now.month})
    result.update({'bot_day': _now.day})
    result.update({'bot_hour': _now.hour})
    result.update({'bot_minute': _now.minute})
    result.update({'bot_second': _now.second})
    return result


def create_date_param():
    _now =  datetime.datetime.now()
    return str(_now.year) + '-' + str(_now.month) + '-' + str(_now.day)


class TwitterSearchDaily:
    def find(self, master_key, text, database, collection):
        master_configuration = setting_datamanager.find_config_by_master_key(master_key)
        
        auth = tweepy.OAuthHandler(master_configuration['CONSUMER_KEY'], master_configuration['CONSUMER_SECRET'])
        auth.set_access_token(master_configuration['OAUTH_TOKEN'], master_configuration['OAUTH_SECRET'])        
        
        data_manager = DataManager(database, collection)
        api = tweepy.API(auth_handler=auth, parser=JSONParser())
        
        switch = True        
        while switch:
            newer_element = data_manager.find_max_element('id')
            
            if 'id' in newer_element:
                max_id = newer_element['id']
                result = api.search(q=text, since_id=max_id, count=100)
            else:
                max_id = 0
                result = api.search(q=text, count=100)

            if 'results' in result:
                for mention in result['results']:
                    if not 'id' in data_manager.select_one_query({'id': mention['id']}):
                        data_manager.create(json.dumps(include_datetime(mention)))            
                
            newer_element = data_manager.find_max_element('id')
            if 'id' in newer_element:
                if max_id == newer_element['id']:
                    break
            else:
                break
            
            time.sleep(10)
        return 'READY'


class TwitterSearchHistory:
    def find(self, master_key, text, database, collection):
        master_configuration = setting_datamanager.find_config_by_master_key(master_key)
        
        auth = tweepy.OAuthHandler(master_configuration['CONSUMER_KEY'], master_configuration['CONSUMER_SECRET'])
        auth.set_access_token(master_configuration['OAUTH_TOKEN'], master_configuration['OAUTH_SECRET'])        
        
        data_manager = DataManager(database, collection)
        api = tweepy.API(auth_handler=auth, parser=JSONParser())
        
        switch = True        
        while switch:
            old_element = data_manager.find_min_element('id')
            
            if 'id' in old_element:
                min_id = old_element['id']
                result = api.search(q=text, max_id=min_id, count=100)
            else:
                min_id = 0
                result = api.search(q=text, count=100)

            if 'results' in result:
                for mention in result['results']:
                    if not 'id' in data_manager.select_one_query({'id' : mention['id']}):
                        data_manager.create(json.dumps(include_datetime(mention)))            
                
            old_element = data_manager.find_min_element('id')
            if 'id' in old_element:
                if min_id == old_element['id']:
                    break                
            else:
                break
            
            time.sleep(10)
        
        return 'READY'


class TwitterProfile:
    def find(self, master_key, profile, database, collection):
        master_configuration = setting_datamanager.find_config_by_master_key(master_key)
        
        auth = tweepy.OAuthHandler(master_configuration['CONSUMER_KEY'], master_configuration['CONSUMER_SECRET'])
        auth.set_access_token(master_configuration['OAUTH_TOKEN'], master_configuration['OAUTH_SECRET'])        
        
        data_manager = DataManager(database, collection)
        api = tweepy.API(auth_handler=auth, parser=JSONParser())

        result = api.get_user(profile)
        if 'id' in result:
            data_manager.create(json.dumps(include_datetime(result)))
            
        return 'READY'


class TwitterTweetsNews:
    def find(self, master_key, profile, database, collection):
        master_configuration = setting_datamanager.find_config_by_master_key(master_key)
        
        auth = tweepy.OAuthHandler(master_configuration['CONSUMER_KEY'], master_configuration['CONSUMER_SECRET'])
        auth.set_access_token(master_configuration['OAUTH_TOKEN'], master_configuration['OAUTH_SECRET'])        
        
        data_manager = DataManager(database, collection)
        api = tweepy.API(auth_handler=auth, parser=JSONParser())        
        
        switch = True
        while switch:
            newer_element = data_manager.find_max_element('id')
            if 'id' in newer_element:
                max_id = newer_element['id']
                result = api.user_timeline(screen_name=profile, count=200, since_id=max_id)
            else:
                max_id = 0
                result = api.user_timeline(screen_name=profile, count=200)
            
            if result.count > 0:
                for tweet in result:
                    if 'id' not in data_manager.select_one_query({'id': tweet['id']}):
                        data_manager.create(json.dumps(include_datetime(tweet)))
            else:
                switch = False
                
            newer_element = data_manager.find_max_element('id')
            if 'id' in newer_element:
                if max_id == newer_element['id']:
                    break
            else:
                break 
                
            time.sleep(10)
            
        return 'READY' 


class TwitterTweetsHistory:
    def find(self, master_key, profile, database, collection):
        master_configuration = setting_datamanager.find_config_by_master_key(master_key)
        
        auth = tweepy.OAuthHandler(master_configuration['CONSUMER_KEY'], master_configuration['CONSUMER_SECRET'])
        auth.set_access_token(master_configuration['OAUTH_TOKEN'], master_configuration['OAUTH_SECRET'])        
        
        api = tweepy.API(auth_handler=auth, parser=JSONParser())
        data_manager = DataManager(database, collection) 
        while data_manager.count_collection() < 5000:
            old_element = data_manager.find_min_element('id')            
            if 'id' in old_element:
                min_id = old_element['id']
                result = api.user_timeline(screen_name=profile, count=200, max_id=min_id)
            else:
                min_id = 0
                result = api.user_timeline(screen_name=profile, count=200)
                
            for tweet in result:
                if not 'id' in data_manager.select_one_query({'id': tweet['id']}):
                    data_manager.create(json.dumps(include_datetime(tweet)))            
            
            old_element = data_manager.find_min_element('id')
            if 'id' in old_element:
                if min_id == old_element['id']:
                    break                
            else:
                break
            
            time.sleep(10)
        return "READY"


class TwitterMentionsAnalyzer:
    def start(self, master_key, database, collection):
        master_configuration = setting_datamanager.find_config_by_master_key(master_key)
        
        analyzed_collection = collection + '_analyzed'
        words_collection = collection + '_words'
        failed_collection = 'failed_analyzer'
        
        data_analyzed = DataManager(database, analyzed_collection)
        data_words = DataManager(database, words_collection)
        data_mentions = DataManager(database, collection)
        data_failed = DataManager(database, failed_collection)
        
        newer_element = data_analyzed.find_max_element('id')
        if ('id' in newer_element):
            max_id = newer_element['id']
        else:
            max_id = 0
            
        mentions = data_mentions.select_collection_query({"id": {"$gt": max_id}})
        
        #TODO: DATETIME
        for mention in mentions:       
            if not 'id' in data_analyzed.select_one_query({'id' : mention['id']}):            
                data_analyzed.create(json.dumps(include_datetime({"id" : mention['id']})))
                words = mention["text"].split()
                
                for word in words:
                    try:
                        data_words.create(json.dumps(include_datetime({"id" : mention['id'], "word" : word, "created_at" : mention["created_at"]})))
                    except:
                        data_failed.create(json.dumps(include_datetime({"id" : mention['id'], "text" : mention['text'], "collection" : collection})))                        
        
        return "READY"


class TwitterRetweetsForTweet:
    def find(self, master_key, database, collection):
        master_configuration = setting_datamanager.find_config_by_master_key(master_key)

        auth = tweepy.OAuthHandler(master_configuration['CONSUMER_KEY'], master_configuration['CONSUMER_SECRET'])
        auth.set_access_token(master_configuration['OAUTH_TOKEN'], master_configuration['OAUTH_SECRET'])

        api = tweepy.API(auth_handler=auth, parser=JSONParser())
        data_tweets = DataManager(database, collection)
        data_retweets = DataManager(database, collection + '_retweets')
        data_retweets_found = DataManager(database, collection + '_retweets_found')

        tweets = data_tweets.select_collection_query({})

        for tweet in tweets:
            if 'id' not in data_retweets_found.select_one_query({'id': tweet['id']}):
                try:
                    retweets = api.retweets(id=tweet['id'], count=100, trim_user=1)

                    for retweet in retweets:
                        if not 'id' in data_retweets.select_one_query({'id' : retweet['id']}):
                            data_retweets.create(json.dumps(include_datetime(retweet)))

                    data_retweets_found.create(json.dumps(include_datetime({'id' : tweet['id']})))
                    print "Found!"
                except:
                    print sys.exc_info()[1]


                time.sleep(20)

        return "READY"
