from monaco.data.setting_data_manager import SettingDataManager
from monaco.data.data_manager import DataManager
from klout import *
import json
import datetime
from time import mktime

setting_datamanager = SettingDataManager()
data_manager = DataManager

class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return int(mktime(obj.timetuple()))
        return json.JSONEncoder.default(self, obj)
    
def include_datetime(result):
    _now =  datetime.datetime.now()
    result.update({'bot_datetime' : json.dumps(_now, cls=DateTimeEncoder)})
    result.update({'bot_year' : _now.year})
    result.update({'bot_month' : _now.month})
    result.update({'bot_day' : _now.day})
    result.update({'bot_hour' : _now.hour})
    result.update({'bot_minute' : _now.minute})
    result.update({'bot_second' : _now.second})
    return result

class KloutScore():
    def find(self, master_key, profile, database, collection):
        master_configuration = setting_datamanager.find_config_by_master_key(master_key)        
        
        k = Klout(master_configuration['OAUTH_TOKEN'])
        
        kloutId = k.identity.klout(screenName=profile).get('id')
        
        if (kloutId > 0):
            data_manager = DataManager(database, collection)
            data_manager.create(json.dumps(include_datetime(k.user.score(kloutId=kloutId))))            
        return 'READY'
    
    
class KloutTopics():
    def find(self, master_key, profile, database, collection):
        master_configuration = setting_datamanager.find_config_by_master_key(master_key)        
        
        k = Klout(master_configuration['OAUTH_TOKEN'])
        
        kloutId = k.identity.klout(screenName=profile).get('id')
        
        if (kloutId > 0):
            data_manager = DataManager(database, collection)            
            result = k.user.topics(kloutId=kloutId)
            for topic in result:
                data_manager.create(json.dumps(include_datetime(topic)))            
        return 'READY'
    
class KloutInfluence():
    def find(self, master_key, profile, database, collection):
        master_configuration = setting_datamanager.find_config_by_master_key(master_key)        
        
        k = Klout(master_configuration['OAUTH_TOKEN'])
        
        kloutId = k.identity.klout(screenName=profile).get('id')
        
        if (kloutId > 0):
            result = k.user.influence(kloutId=kloutId)
            data_manager = DataManager(database, collection)
            data_manager.create(json.dumps(include_datetime(result)))            
        return 'READY'